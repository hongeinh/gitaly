package featureflag

// GitV2360Gl1 will enable use of Git v2.36.0.gl1.
var GitV2360Gl1 = NewFeatureFlag("git_v2360gl1", false)
